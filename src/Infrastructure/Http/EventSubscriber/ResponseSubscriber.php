<?php

declare(strict_types=1);

namespace App\Infrastructure\Http\EventSubscriber;

use JMS\Serializer\SerializerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class ResponseSubscriber implements EventSubscriberInterface
{
    /**
     * @var SerializerInterface
     */
    private $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['onKernelView', 30],
        ];
    }

    public function onKernelView(ViewEvent $event): void
    {
        $data = $event->getControllerResult();

        if (!$data) {
            $response = new JsonResponse(null, JsonResponse::HTTP_NO_CONTENT);
        } elseif ($data instanceof Response) {
            $response = $data;
        } else {
            $response = (new JsonResponse())->setJson($this->serializer->serialize($data, 'json'));
        }

        $event->setResponse($response);
    }
}
