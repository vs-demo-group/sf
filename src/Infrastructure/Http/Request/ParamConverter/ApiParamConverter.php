<?php

declare(strict_types=1);

namespace App\Infrastructure\Http\Request\ParamConverter;

use JMS\Serializer\ArrayTransformerInterface;
use JMS\Serializer\SerializerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;

//use Symfony\Component\Validator\Validator\ValidatorInterface;

class ApiParamConverter implements ParamConverterInterface
{
    const CONVERTER_NAME = 'api';

    /**
     * @var SerializerInterface|ArrayTransformerInterface
     */
    private $serializer;

//    /**
//     * @var ValidatorInterface
//     */
//    private $validator;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * {@inheritdoc}
     */
    public function apply(Request $request, ParamConverter $configuration)
    {
        if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
            $data = json_decode($request->getContent(), true);
            $request->request->replace(is_array($data) ? $data : []);
        }

        $requestData = $this->getData($request);
        $dto         = $this->serializer->fromArray($requestData, $configuration->getClass());
        $request->attributes->set($configuration->getName(), $dto);
//        $options = $configuration->getOptions();
//        $errors  = $this->validator->validate($dto, null, $options['validator_groups'] ?? null);

//        if ($errors->count()) {
//            if ($options['throw_exception'] ?? true) {
//                throw new ParamConverterValidationException($errors);
//            } else {
//                $request->attributes->set(self::VIOLATIONS_PARAM_NAME, $errors);
//            }
//        }
    }

    /**
     * {@inheritdoc}
     */
    public function supports(ParamConverter $configuration)
    {
        return $configuration->getClass() && (self::CONVERTER_NAME === $configuration->getConverter());
    }

    protected function getData(Request $request): array
    {
        return array_merge($request->attributes->all(), $request->request->all(), $this->replaceReservedSymbols($request->query->all()));
    }

    private function replaceReservedSymbols(array $params): array
    {
        return array_map(function ($param) {
            return $this->replace($param);
        }, $params);
    }

    private function replace($param)
    {
        if (is_array($param)) {
            return array_map(function ($value) {
                return $this->replace($value);
            }, $param);
        }

        // [' ', '+', '.', ',', ';', '?', '|', '*', '/', '%', '^', '$', '#', '@', '[', ']'];
        try {
            $replacements = [
                '/%20/i' => ' ',
                '/%40/i' => '@',
                '/%3A/i' => ':',
                '/%24/i' => '$',
                '/%2C/i' => ',',
                '/%3B/i' => ';',
                '/%2B/i' => '+',
                '/%23/i' => '#',
                '/%3D/i' => '=',
                '/%3F/i' => '?',
                '/%2F/i' => '/',
            ];

            $result = (string) $param;

            foreach ($replacements as $pattern => $replace) {
                $result = preg_replace($pattern, $replace, $result);
            }

            return $result;
        } catch (\Exception $x) {
            return $param;
        }
    }
}
