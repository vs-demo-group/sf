<?php

declare(strict_types=1);

namespace App\Infrastructure\Http\Controller\Api;

use App\Application\Command\User\CreateUserCommand;
use App\Application\Command\User\LoginCommand;
use App\Application\UseCase\User\CreateUser;
use App\Application\UseCase\User\LoginUser;
use App\Application\ViewModel\User\LoginUserView;
use App\Application\ViewModel\User\RegistredUserView;
use Nelmio\ApiDocBundle\Annotation\Security as NelmioSecurity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class UserController
{
    /**
     * @NelmioSecurity(name="AuthToken")
     *
     * @SWG\Response(response=200, description="secured route to demonstrate that auth works")
     * @Route(name="secret", path="/api/secret", methods={"GET"})
     *
     * @return Response|JsonResponse
     */
    public function secret(Security $security)
    {
        return new Response('Hi '.$security->getUser()->getUsername());
    }

    /**
     * @Route("/api/login", methods={"POST"})
     *
     * @SWG\Parameter(name="body", in="body", required=true,
     *     @SWG\Schema(ref="#/definitions/User")
     * )
     *
     * @SWG\Tag(name="Users")
     * @SWG\Response(response=200, description="user login action",
     *  @SWG\Schema(ref="#/definitions/LoginUserView")
     * )
     *
     * @ParamConverter("command", converter="api")
     */
    public function login(LoginCommand $command, LoginUser $loginUserUseCase): LoginUserView
    {
        $user = $loginUserUseCase->execute($command);

        return LoginUserView::create($user);
    }

    /**
     * @Route("/api/reg", methods={"POST"})
     *
     * @SWG\Parameter(name="body", in="body", required=true,
     *     @SWG\Schema(ref="#/definitions/User")
     * )
     * @SWG\Tag(name="Users")
     * @SWG\Response(response=200, description="user registration action",
     *  @SWG\Schema(ref="#/definitions/RegistredUserView")
     * )
     *
     * @ParamConverter("createUserCommand", converter="api")
     */
    public function register(CreateUserCommand $createUserCommand, CreateUser $createUserUseCase): RegistredUserView
    {
        $user = $createUserUseCase->execute($createUserCommand);

        return RegistredUserView::create($user);
    }
}
