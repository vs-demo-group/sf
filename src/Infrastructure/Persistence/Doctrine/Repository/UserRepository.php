<?php

declare(strict_types=1);

namespace App\Infrastructure\Persistence\Doctrine\Repository;

use App\Application\Repository\UserRepositoryInterface;
use App\Domain\User\Model\UserInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

class UserRepository extends EntityRepository implements UserRepositoryInterface
{
    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function save(UserInterface $user): void
    {
        $this->_em->persist($user);
        $this->_em->flush();
    }

    /**
     * @return UserInterface|object|null
     */
    public function findOneByEmail(string $email): ?UserInterface
    {
        return $this->findOneBy(['credentials.email' => $email]);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function update(UserInterface $user): void
    {
        $this->_em->flush();
    }
}
