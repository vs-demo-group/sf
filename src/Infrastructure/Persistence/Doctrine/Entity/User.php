<?php

declare(strict_types=1);

namespace App\Infrastructure\Persistence\Doctrine\Entity;

use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class User extends \App\Domain\User\Model\User implements UserInterface, EquatableInterface
{
    /**
     * {@inheritdoc}
     */
    public function getRoles(): ?array
    {
        return ['ROLE_USER'];
    }

    /**
     * {@inheritdoc}
     */
    public function getPassword(): ?string
    {
        return (string) $this->credentials->hashedPassword();
    }

    /**
     * {@inheritdoc}
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function getUsername(): string
    {
        return $this->credentials->email();
    }

    /**
     * {@inheritdoc}
     */
    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    public function isEqualTo(UserInterface $user)
    {
        if (!$user instanceof UserInterface) {
            return false;
        }

        if ((string) $this->credentials->hashedPassword() !== (string) $user->getPassword()) {
            return false;
        }

        if ($this->credentials->email() !== $user->getUsername()) {
            return false;
        }

        return true;
    }
}
