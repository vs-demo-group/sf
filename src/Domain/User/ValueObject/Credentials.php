<?php

declare(strict_types=1);

namespace App\Domain\User\ValueObject;

class Credentials
{
    private HashedPassword $hashedPassword;

    private string $email;

    public function __construct(string $email, HashedPassword $hashedPassword)
    {
        $this->email          = $email;
        $this->hashedPassword = $hashedPassword;
    }

    public function email(): string
    {
        return $this->email;
    }

    public function hashedPassword(): HashedPassword
    {
        return $this->hashedPassword;
    }
}
