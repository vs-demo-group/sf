<?php

declare(strict_types=1);

namespace App\Domain\User\ValueObject;

class HashedPassword
{
    const COST = 12;

    private string $hashedPassword;

    public function __toString(): string
    {
        return $this->hashedPassword;
    }

    public static function fromHash(string $hashedPassword): self
    {
        $pass                 = new self();
        $pass->hashedPassword = $hashedPassword;

        return $pass;
    }

    public static function encode(string $plainPassword): self
    {
        $pass = new self();
        $pass->hash($plainPassword);

        return $pass;
    }

    public function match(string $plainPassword): bool
    {
        return password_verify($plainPassword, $this->hashedPassword);
    }

    private function hash(string $plainPassword): void
    {
        $this->hashedPassword = password_hash($plainPassword, PASSWORD_BCRYPT, ['cost' => self::COST]);
    }
}
