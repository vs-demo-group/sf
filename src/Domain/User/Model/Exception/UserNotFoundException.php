<?php

declare(strict_types=1);

namespace App\Domain\User\Model\Exception;

class UserNotFoundException extends \Exception
{
    public function __construct()
    {
        parent::__construct('user.exception.not_found');
    }
}
