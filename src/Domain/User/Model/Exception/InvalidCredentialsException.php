<?php

declare(strict_types=1);

namespace App\Domain\User\Model\Exception;

class InvalidCredentialsException extends \Exception
{
    public function __construct()
    {
        parent::__construct('user.exception.invalid_credentials');
    }
}
