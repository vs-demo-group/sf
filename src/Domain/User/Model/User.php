<?php

declare(strict_types=1);

namespace App\Domain\User\Model;

use App\Domain\User\ValueObject\Credentials;

class User implements UserInterface
{
    protected int $id;
    protected string $apiToken;
    protected Credentials $credentials;

    public function __construct(Credentials $credentials)
    {
        $this->credentials = $credentials;
    }

    /**
     * @return string
     */
    public function apiToken(): ?string
    {
        return $this->apiToken;
    }

    public function email(): string
    {
        return $this->credentials->email();
    }

    public function credentials(): Credentials
    {
        return $this->credentials;
    }

    public function id(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function generateApiToken(): void
    {
        try {
            $bytes          = random_bytes(5);
            $this->apiToken = bin2hex($bytes);
        } catch (\Exception $e) {
        }
    }
}
