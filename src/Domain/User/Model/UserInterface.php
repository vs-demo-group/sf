<?php

declare(strict_types=1);

namespace App\Domain\User\Model;

interface UserInterface
{
    public function generateApiToken(): void;

    public function email(): string;

    public function apiToken(): ?string;
}
