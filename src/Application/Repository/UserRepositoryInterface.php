<?php

declare(strict_types=1);

namespace App\Application\Repository;

use App\Domain\User\Model\UserInterface;

interface UserRepositoryInterface
{
    public function save(UserInterface $user): void;

    public function update(UserInterface $user): void;

    public function findOneByEmail(string $email): ?UserInterface;
}
