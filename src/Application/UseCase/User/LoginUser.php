<?php

declare(strict_types=1);

namespace App\Application\UseCase\User;

use App\Application\Command\User\LoginCommand;
use App\Application\Repository\UserRepositoryInterface;
use App\Domain\User\Model\Exception\InvalidCredentialsException;
use App\Domain\User\Model\Exception\UserNotFoundException;
use App\Domain\User\Model\User;
use App\Domain\User\Model\UserInterface;

class LoginUser
{
    private UserRepositoryInterface $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @throws InvalidCredentialsException
     * @throws UserNotFoundException
     */
    public function execute(LoginCommand $command): ?UserInterface
    {
        /** @var User $user */
        $user = $this->userRepository->findOneByEmail($command->email);

        if (!$user) {
            throw new UserNotFoundException();
        }

        if (!$user->credentials()->hashedPassword()->match($command->password)) {
            throw new InvalidCredentialsException();
        }

        $user->generateApiToken();
        $this->userRepository->save($user);

        return $user;
    }
}
