<?php

declare(strict_types=1);

namespace App\Application\UseCase\User;

use App\Application\Command\User\CreateUserCommand;
use App\Application\Repository\UserRepositoryInterface;
use App\Domain\User\Model\User;
use App\Domain\User\Model\UserInterface;
use App\Domain\User\ValueObject\Credentials;
use App\Domain\User\ValueObject\HashedPassword;

class CreateUser
{
    private UserRepositoryInterface $repository;

    public function __construct(UserRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function execute(CreateUserCommand $command): UserInterface
    {
        $credentials = new Credentials($command->email, HashedPassword::encode($command->password));
        $user        = new User($credentials);
        $this->repository->save($user);

        return $user;
    }
}
