<?php

declare(strict_types=1);

namespace App\Application\Command\User;

class LoginCommand
{
    public string $email;
    public string $password;

    /**
     * LoginCommand constructor.
     *
     * @param $email
     * @param $password
     */
    public function __construct(string $email, string $password)
    {
        $this->email    = $email;
        $this->password = $password;
    }
}
