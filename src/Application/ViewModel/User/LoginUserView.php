<?php

declare(strict_types=1);

namespace App\Application\ViewModel\User;

use App\Domain\User\Model\UserInterface;

class LoginUserView
{
    private string $email;

    private string $apiToken;

    public function __construct(string $email, string $apiToken)
    {
        $this->email    = $email;
        $this->apiToken = $apiToken;
    }

    public static function create(UserInterface $user)
    {
        return new self($user->email(), $user->apiToken());
    }
}
