<?php

declare(strict_types=1);

namespace App\Application\ViewModel\User;

use App\Domain\User\Model\UserInterface;

class RegistredUserView
{
    protected string $email;

    public function __construct(string $email)
    {
        $this->email = $email;
    }

    public static function create(UserInterface $user)
    {
        return new self($user->email());
    }
}
