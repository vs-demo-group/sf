<?php

declare(strict_types=1);

namespace App\Tests\Application\UseCase;

use App\Application\Command\User\LoginCommand;
use App\Application\Repository\UserRepositoryInterface;
use App\Application\UseCase\User\LoginUser;
use App\Domain\User\Model\Exception\InvalidCredentialsException;
use App\Domain\User\Model\Exception\UserNotFoundException;
use App\Domain\User\Model\User;
use App\Domain\User\ValueObject\Credentials;
use App\Domain\User\ValueObject\HashedPassword;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

class LoginUserTest extends TestCase
{
    /** @noinspection PhpStrictTypeCheckingInspection */
    public function testExecute()
    {
        $hashedPassword = $this->prophesize(HashedPassword::class);
        $hashedPassword->match(Argument::type('string'))->shouldBeCalled()->willReturn(true);

        $credentials = $this->prophesize(Credentials::class);
        $credentials->hashedPassword()->shouldBeCalled()->willReturn($hashedPassword->reveal());

        $user = $this->prophesize(User::class);
        $user->credentials()->shouldBeCalled()->willReturn($credentials->reveal());
        $user->generateApiToken()->shouldBeCalled();

        $repo = $this->prophesize(UserRepositoryInterface::class);
        $repo->findOneByEmail(Argument::type('string'))->shouldBeCalled()->willReturn($user->reveal());
        $repo->save(Argument::type(User::class));

        $useCase = new LoginUser($repo->reveal());
        $useCase->execute(new LoginCommand('email@email.com', 'foo'));

        self::assertNotNull($user->apiToken());
    }

    /** @noinspection PhpStrictTypeCheckingInspection */
    public function testUserNotFoundException(): void
    {
        self::expectException(UserNotFoundException::class);

        $repo = $this->prophesize(UserRepositoryInterface::class);
        $repo->findOneByEmail(Argument::type('string'))->shouldBeCalled()->willReturn(null);
        $useCase = new LoginUser($repo->reveal());
        $useCase->execute(new LoginCommand('email@email.com', 'foo'));
    }

    /** @noinspection PhpStrictTypeCheckingInspection */
    public function testInvalidCredentialsException(): void
    {
        self::expectException(InvalidCredentialsException::class);
        $hashedPassword = $this->prophesize(HashedPassword::class);
        $hashedPassword->match(Argument::type('string'))->shouldBeCalled()->willReturn(false);

        $credentials = $this->prophesize(Credentials::class);
        $credentials->hashedPassword()->shouldBeCalled()->willReturn($hashedPassword->reveal());

        $user = $this->prophesize(User::class);
        $user->credentials()->shouldBeCalled()->willReturn($credentials->reveal());

        $repo = $this->prophesize(UserRepositoryInterface::class);
        $repo->findOneByEmail(Argument::type('string'))->shouldBeCalled()->willReturn($user->reveal());
        $useCase = new LoginUser($repo->reveal());
        $useCase->execute(new LoginCommand('email@email.com', 'foo'));
    }
}
