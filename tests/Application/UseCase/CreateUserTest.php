<?php

declare(strict_types=1);

namespace App\Tests\Application\UseCase;

use App\Application\Command\User\CreateUserCommand;
use App\Application\Repository\UserRepositoryInterface;
use App\Application\UseCase\User\CreateUser;
use App\Domain\User\Model\UserInterface;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

class CreateUserTest extends TestCase
{
    public function testExecute()
    {
        $repo = $this->prophesize(UserRepositoryInterface::class);
        $repo->save(Argument::type(UserInterface::class))->shouldBeCalled();

        $useCase = new CreateUser($repo->reveal());
        $useCase->execute(new CreateUserCommand('email@email.com', 'foo'));
    }
}
