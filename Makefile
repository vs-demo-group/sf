.SILENT:

## Colors
COLOR_RESET   = \033[0m
COLOR_INFO    = \033[32m
COLOR_COMMENT = \033[33m


lint:
	vendor/bin/php-cs-fixer --allow-risky=yes fix

phpunit:
	bin/phpunit tests/
	php coverage-checker.php report/clover.xml ${COVERAGE_LIMIT_PERCENT}

phpunit-no-xdebug:
	php -c ./disable-xdebug.ini bin/phpunit tests/

##  make tag v=1.0.1
tag:
	git tag v$(v)
	git push --tags

